<?xml version="1.0"?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="no" />

  <!-- IdentityTransform -->
  <xsl:template match="/">
    <xsl:apply-templates select="@*|node()" />
  </xsl:template>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="/xdxf/lexicon/ar/def/def">
    <xsl:copy-of select="gr" />
    <co type="spv">
      <xsl:value-of select="./@def-id" />
    </co>
    <xsl:copy-of select="def" />
  </xsl:template>

</xsl:stylesheet>
