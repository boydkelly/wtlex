#!/usr/bin/bash
#set -o errexit
set -o nounset
set -o pipefail

for x in jq yq csvclean brainbrew pyglossary; do
  type -P $x >/dev/null 2>&1 || {
    echo >&2 "${x} not installed.  Aborting."
    exit 0
  }
done

set -o errexit
file=wtlex
project=wtlex
for x in tsv tmp xml xsd xsl adoc json yml slob; do export "${x}=${file}.$x"; done
build=build
src=src
dict="./$build/dict.xdxf"
branch=dev
webtarget="$HOME"/dev/svelte/coastsystems.net/src
dyu_lex="$HOME"/dev/svelte/dyu-lex
source ./functions.sh

checkbranch "$webtarget" "$branch"
checkbranch "$dyu_lex" "$branch"

[[ -d "$build" ]] || mkdir "$build"
[[ -f "$build/$xml" ]] && count_xml "$build/$xml"

DATE=$(date -I) && export DATE
yq -i '.xdxf.meta_info.last_edited_date = strenv(DATE)' "$yml"
yamlfmt "$yml"

#make new xml xdxf from yml
yq -oj "$yml" >"$build/$json"
yq -ox "$yml" >"$dict"
ln -srvf $src/xdxf_strict.dtd $build/
xmllint --noout --dtdvalid $src/xdxf_strict.dtd $dict
ln -v -f $dict "./$build/$xml"

#now we have a new xml file.  sort it back to the yaml
#xslt3 -xsl:$build-sort.xsl -s:"$build/$xml" >"~sorted-$xml"
#make sure that all k elements are converted back to arrays
#yq does not have if statement...
xslt3 -xsl:$project-sort.xsl -s:"$build/$xml" |
  yq -px -oy '
  .xdxf.lexicon.ar[].def |= ([] + .) |
  .xdxf.lexicon.ar[].def[].def |= ([] + .) |
  .xdxf.lexicon.ar[].def[].def[].def |= ([] + .) |
  .xdxf.lexicon.ar[].def[].def[].def[].def |= ([] + .) |
  .xdxf.lexicon.ar[].k |= ([] + .) |
  .xdxf.lexicon.ar[].def[].def[].gr |= ([] + .) |
  .xdxf.lexicon.ar[].def[].def[].def[0].def[].ex |= ([] + .) |
  .xdxf.lexicon.ar[].def[].def[].def[0].def[] |= pick(["deftext", "ex", "sr", "categ"]) |
  .xdxf.lexicon.ar[].def[].def[].def[0].def[].ex[0].+@type // "foo" |= . |
  .xdxf.lexicon.ar[].def[].def[].def[0].def[].ex[0].ex_orig // "bar" |= . |
 .xdxf.lexicon.ar[].def[].def[].def[0].def[].ex[0] |= select(.["+@type"] == null) |= .ex_orig = "" |
 .xdxf.lexicon.ar[].def[].def[].def[0].def[].ex[0] |= select(.ex_orig == "") |= .+@type = "exm"
' >"new-$yml"
#' "~sorted-$xml" >"new-$yml"
#add these later as they blow up second time round
#yq adds a null valut to kref wich is ok but the next script run the // will change the nul to []
# and this does not pass the dtd
# investigate testing for content and if none then delete???
# .xdxf.lexicon.ar[].def[].def[].def[0].def[].sr.kref |= ([] + .) |
# .xdxf.lexicon.ar[].def[].def[].def[0].def[].categ |= ([] + .) |

#count_xml "$build/$xml"
#count_xml "~sorted-$xml"
count_yml "$yml"
#count_yml "new-$yml"
count_yml "$yml"
mv -v "new-$yml" "$yml"
cp -v "$yml" "./archive/$(date -Im)-$yml"
#sed -i "2i +directive: \"DOCTYPE xdxf SYSTEM 'xdxf_strict.dtd'\"" "$yml"
sed -i "2i +directive: DOCTYPE xdxf SYSTEM 'xdxf_strict.dtd'" "$yml"
count_yml "$yml"
count_xml "$build/$xml"
yamlfmt "$yml"
#rm ./~*

#wtlex specific Anki/brainbrew
xslt3 "-xsl:$file-tsv.xsl" -s:"$dict" >"$build/$tsv"
count_tsv "$build/$tsv"
validate "$build/$tsv"
cp -v "$build/$tsv" /var/home/bkelly/Anki/brainbrew/WT/src/data/Main.tsv
ln -sf /var/home/bkelly/Anki/brainbrew/WT/src/data/Main.tsv $build
yq -py -oj "$yml" >"$build/$json"

#pyglossary
[[ -f "$build/$slob" ]] && rm "$build/$slob"
pyglossary "$dict" "$build/$slob"
cp -v "$build/$slob" "$HOME/drive/Sync/apk/"

#make a json file and convert objects to arrays website carbon table etc
# yq -px -oj "$build/$xml" >"$build/$json"

# yq -px -oj "$build/$xml" |
#   jq '.xdxf.lexicon.ar |= map(if .k | type != "array" then .k = [.k] else . end)' |
#   jq 'def ensure_def_is_array:
#     if type == "object" then
#         . as $in
#         | reduce keys[] as $key
#           ({}; . + { ($key): ($in[$key] | ensure_def_is_array) })
#         | if has("def") then
#               .def |= if type == "array" then map(ensure_def_is_array) else [ensure_def_is_array] end
#           else .
#           end
#     elif type == "array" then
#         map(ensure_def_is_array)
#     else .
#     end;
#
# this works with jq, but if yq can do it lets stay with that.
# walk(if type == "object" and has("def") then ensure_def_is_array else . end)' >"$build/$json"
# this does the defs, the k and the ex but the output is not in the right order... ;(
# yq -px -oj "$build/$xml" |
#   jq 'def ensure_elements_are_arrays:
#     if type == "object" then
#         . as $in
#         | reduce keys[] as $key
#           ({}; . + { ($key): ($in[$key] | ensure_elements_are_arrays) })
#         | if has("def") then
#               .def |= if type == "array" then map(ensure_elements_are_arrays) else [ensure_elements_are_arrays] end
#           else .
#           end
#         | if has("k") and (.k | type != "array") then
#               .k = [.k]
#           else .
#           end
#         | if has("ex") and (.ex | type != "array") then
#               .ex = [.ex]
#           else .
#           end
#         | if has("gr") and (.gr | type != "array") then
#               .gr = [.gr]
#           else .
#           end
#         | if has("categ") and (.categ | type != "array") then
#               .categ = [.categ]
#           else .
#           end
#     elif type == "array" then
#         map(ensure_elements_are_arrays)
#     else .
#     end;
#     walk(if type == "object" then ensure_elements_are_arrays else . end)' \
#     >"$build/$json"

#this is also unneeded as if the arrays are in yml they will be exported to json at the beginning.
# xslt3 -xsl:$build-sort.xsl -s:"$build/$xml" |
#   yq -px -oj '
#   .xdxf.lexicon.ar[].def |= ([] + .) |
#   .xdxf.lexicon.ar[].def[].def |= ([] + .) |
#   .xdxf.lexicon.ar[].def[].def[].def |= ([] + .) |
#   .xdxf.lexicon.ar[].def[].def[].def[].def |= ([] + .) |
#   .xdxf.lexicon.ar[].k |= ([] + .) |
#   .xdxf.lexicon.ar[].def[].def[].gr |= ([] + .) |
#   .xdxf.lexicon.ar[].def[].def[].def[0].def[].ex |= ([] + .) |
#   .xdxf.lexicon.ar[].def[].def[].def[0].def[] |= pick(["deftext", "ex", "sr", "categ"]) |
#   .xdxf.lexicon.ar[].def[].def[].def[0].def[].ex[0].+@type // "foo" |= . |
#   .xdxf.lexicon.ar[].def[].def[].def[0].def[].ex[0].ex_orig // "bar" |= . |
#  .xdxf.lexicon.ar[].def[].def[].def[0].def[].ex[0] |= select(.["+@type"] == null) |= .ex_orig = "" |
#  .xdxf.lexicon.ar[].def[].def[].def[0].def[].ex[0] |= select(.ex_orig == "") |= .+@type = "exm"
# ' >"$build/$json"
#website adoc file
target=/var/home/bkelly/dev/svelte/coastsystems.net/src

#adoc version
for x in en fr ; do
xslt3 -xsl:$file-adoc.xsl -s:$build/"$xml" >"$target/_include/$x/vocab-wt.adoc"
done
