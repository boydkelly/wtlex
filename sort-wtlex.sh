#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail

project=wtlex

#[ -f ${HEADWORDS} ] || { echo "${HEADWORDS} not found.  Exiting..."; exit 1; }

for x in tmp xml xsd yml adoc json xls; do export "${x}=${project}.$x"; done

for x in jq xq; do
  type -P $x >/dev/null 2>&1 || {
    echo >&2 "${x} not installed.  Aborting."
    exit 1
  }
done

function count {
  # cat $1 | awk -v file=$1 '{n += gsub(/\<ar\>/, "")} END {print n " entries in " file}'
  echo $(sed -n '/<ar>/p' $1 | wc -l) entries
}

xmllint --noout --dtdvalid xdxf_strict.dtd "$project/$xml"
count "$project/$xml"
xslt3 -xsl:$project-sort.xsl -s:$project/$xml >sorted-$tmp
count sorted-$tmp
yq -px <sorted-$tmp | yq -oy >new-$yml
rm *.tmp
