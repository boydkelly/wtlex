#!/usr/bin/bash
cmd="uuidgen"
#
tsv="./Main.tsv"
out="./dp.yml"
rm $out
awk -v out=$out -v cmd="$cmd" -v uuid="$($cmd)" -F '\t' 'BEGIN { test="out.yaml"; }
                 { NR>1 }
                 { uuid4 = ((cmd | getline xout) > 0 ? xout : uuid) }
                 { close(cmd) }

                 { if ($64 ~ /dp/)
                   {
                    printf ("                      - \047@type\047: exm\n") >> out;
                    printf ("                        \047#author\047: \"%s\"\n", $3) >> out;
                    printf ("                        \047@source\047: \"%s\"\n", $16) >> out;
                    printf ("                        ex_orig: \"%s\"\n", $2) >> out;
                    printf ("                        ex_tran: \"%s\"\n", $5) >> out;
                 }

                 }' $tsv 
