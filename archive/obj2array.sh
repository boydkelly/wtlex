#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail

project=project

#[ -f ${HEADWORDS} ] || { echo "${HEADWORDS} not found.  Exiting..."; exit 1; }

for x in tmp xml xsd yaml adoc json; do export "${x}=${project}.$x"; done

for x in ls echo ; do
  type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

xq < dict.xml \
  | jq '(.xdxf.lexicon.ar[].def.def.def) | if type=="array" then map(.[]) else . |= [.] end' > test1.json

#.[] else . end' > test1.json

xq < dict.xml \
  | jq . > test2.json

#
  #| jq '.xdxf.lexicon.ar[].def.def.def | if type=="object" then (. |= [ . ]) else .[] end' > test1.json
#... | if type=="string" then [.] else . end | .[] | ...

#    = [ (.def |= [ . ]) ]'
#  | jq '.xdxf.lexicon.ar.def.def.def |= [ (.def |= [ . ]) ]'
# if type!="array" then [.] else . end
