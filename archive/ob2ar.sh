#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail

project=project

#works for k element
yq -py -oj ./wtlex/wtlex.yml |
  jq '.xdxf.lexicon.ar |= map(if .k | type != "array" then .k = [.k] else . end)' >defk.json

jq 'def ensure_def_is_array:
    if type == "object" then
        . as $in
        | reduce keys[] as $key
          ({}; . + { ($key): ($in[$key] | ensure_def_is_array) })
        | if has("def") then
              .def |= if type == "array" then map(ensure_def_is_array) else [ensure_def_is_array] end
          else .
          end
    elif type == "array" then
        map(ensure_def_is_array)
    else .
    end;

walk(if type == "object" and has("def") then ensure_def_is_array else . end)' <defk.json >output.json
