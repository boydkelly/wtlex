#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail
file=wtlex
for x in tsv tmp xml xsd xsl adoc json yml; do export "${x}=${file}.$x"; done
project=wtlex

#yq -p=csv -oj money.csv | yq -pj -oj '.[] |= (.id = .numeric)' >money.json
cp ./wtlex/wtlex.json "./$json"
#jq '.xdxf.lexicon.ar |= map(if .k | type != "array" then .k = [.k] else . end)' <$json >~tmp-$json
#jq '.xdxf.lexicon.ar.def |= map(if .def | type != "array" then .def = [.def] else . end)' <$json >~tmp-$json

jq '
  .xdxf.lexicon.ar |= map(
    # Level 3 `def` elements
    if .def and .def | type != "array" then .def = [.def] else . end |
    # Level 4 `def` elements
    if .def then
      .def |= map(
        if .def and .def | type != "array" then .def = [.def] else . end
      )
    else . end
  )
' <"$json" >~tmp-"$json"
jq '
  .xdxf.lexicon.ar |= map(
    # Level 3 `def` elements
    if .def then
      .def |= if type != "array" then [.]
              else map(
                # Level 4 `def` elements
                if .def and .def | type != "array" then .def = [.def] else . end
              )
              end
    else .
    end
  )
' <"$json" >~tmp2-"$json"
jq '
  .xdxf.lexicon.ar |= map(
    . |
    # Traverse to the first level of objects
    (
      if has("def") then
        .def |= if type != "array" then [.] else . end |
        # Traverse to the second level of objects within def
        .def |= map(
          if has("def") then
            .def |= if type != "array" then [.] else . end
          else .
          end
        )
      else .
      end
    )
  )
' <"$json" >~tmp2-"$json"
jq '
  .xdxf.lexicon.ar |= map(
    # Ensure we only map elements that contain `def` at level 2
    if has("def") then
      .def |= map(
        # Modify level 3 `def` elements if they exist
        if has("def") then
          .def |= if type != "array" then [.] else . end |
          # Modify level 4 `def` elements if they exist
          .def |= map(
            if has("def") then
              .def |= if type != "array" then [.] else . end
            else .
            end
          )
        else .
        end
      )
    else .
    end
  )
' <"$json" >~tmp3-"$json"

input.json >tmp.json && mv tmp.json input.json

# yq -oy -i '.xdxf.lexicon.ar.[].def.def |= (.co)' test.yml
# yq -oy -i '.xdxf.lexicon.ar.[].def.def.co |= (.+@type = (uuid))' test.yml
# yq -oy -i '.xdxf.lexicon.ar.[].def.def.co |= (.+content = (test))' test.yml
