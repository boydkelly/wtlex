#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail
#seesm to work for everying def, k and ex but reorders
yq -px -oj ./wtlex/wtlex.xml >./wtlex.json
jq 'def ensure_elements_are_arrays:
    if type == "object" then
        . as $in
        | reduce keys[] as $key
          ({}; . + { ($key): ($in[$key] | ensure_elements_are_arrays) })
        | if has("def") then
              .def |= if type == "array" then map(ensure_elements_are_arrays) else [ensure_elements_are_arrays] end
          else .
          end
        | if has("k") and (.k | type != "array") then
              .k = [.k]
          else .
          end
        | if has("ex") and (.ex | type != "array") then
              .ex = [.ex]
          else .
          end
    elif type == "array" then
        map(ensure_elements_are_arrays)
    else .
    end;

    walk(if type == "object" then ensure_elements_are_arrays else . end)' <wtlex.json >output.json
