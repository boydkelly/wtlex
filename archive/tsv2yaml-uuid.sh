#!/usr/bin/bash
cmd="uuidgen"
#
tsv="/var/home/bkelly/Anki/brainbrew/WT/src/data/Main.tsv"
out="./wtlex.yml"

cat <<'EOF' > $out
xdxf:
  '@revision': '034'
  meta_info:
    languages:
      from:
        '@xml:lang': dyu
      to:
        - '@xml:lang': en
        - '@xml:lang': bm
        - '@xml:lang': fr
    title: Lexique WT Mandenkan 
    full_title: Lexique WT Mandenkan 
    description: null
    file_ver: '001'
    creation_date: '2022-08-17'
    last_edited_date: '2022-08-17'
    abbreviations:
      abbr_def:
        - abbr_k: adj.
          abbr_v: adjectif
        - abbr_k: n.
          abbr_v: nom
        - abbr_k: v.
          abbr_v: verbe
        - abbr_k: vi.
          abbr_v: verbe intransitif
        - abbr_k: vt
          abbr_v: verbe transitif
        - abbr_k: post.
          abbr_v: postposition
  lexicon:
    ar:
EOF
awk -v out=$out -v cmd="$cmd" -v uuid="$($cmd)" -F '\t' 'BEGIN { test="out.yaml"; }
                 { NR>1 }
                 { uuid4 = ((cmd | getline xout) > 0 ? xout : uuid) }
                 { close(cmd) }
                 { print  "      - k:" >> out}
                 { print  "          - " "\047" "@xml:lang" "\047" ": dyu" >> out}
                 { printf ("            \047#text\047: \"%s\"\n", $2) >> out}
                 { print  "          - " "\047" "@xml:lang" "\047" ": dyu_CI" >> out}
                 { printf ("            \047#text\047: \"%s\"\n", $20) >> out}
                 { print  "        def: " >> out}
                 { print  "          " "\047" "@cmt"  "\047" ": " $3 >> out}

                 { if ($64 ~ /dp/) {
                   printf ("          \047@freq\047: \"%s\"\n", $16) >> out;
                 }
                 }

                 { print  "          def: " >> out}
                 { printf ("            \047@def-id\047: \"%s\"\n", $1) >> out}
                 { print  "            gr: " >> out}
                 { printf ("              abbr: %s\n", $4) >> out}
                 { print  "            def: " >> out}
                 { print  "                - " "\047" "@xml:lang" "\047" ": fr" >> out}
                 { print  "                  def: " >> out}
                 { printf ("                    deftext: \"%s\"\n", $5) >> out}

                 { if ($27)
                   {
                    print  "                    ex:" >> out;
                    print  "                      - " "\047" "@type" "\047" ": exm" >> out;
                    print  "                        " "\047" "@source"  "\047" ": " $3 >> out;
                    print  "                        ex_orig: " $27 >> out;
                    print  "                        ex_tran: " $28 >> out;
                 }
                 }

                 { if ($29)
                   {
                    print  "                      - " "\047" "@type" "\047" ": exm" >> out;
                    print  "                        " "\047" "@source"  "\047" ": " $3 >> out;
                    print  "                        ex_orig: " $29 >> out;
                    print  "                        ex_tran: " $30 >> out;
                 }
                 }

                 { if ($31)
                   {
                    print  "                      - " "\047" "@type" "\047" ": exm" >> out;
                    print  "                        " "\047" "@source"  "\047" ": " $3 >> out;
                    print  "                        ex_orig: " $31 >> out;
                    print  "                        ex_tran: " $32 >> out;
                 }
                 }

                 { if ($33)
                   {
                    print  "                      - " "\047" "@type" "\047" ": exm" >> out;
                    print  "                        " "\047" "@source"  "\047" ": " $3 >> out;
                    print  "                        ex_orig: " $33 >> out;
                    print  "                        ex_tran: " $34 >> out;
                 }
                 }

                 { print  "                    sr: " >> out}
                 { print  "                      kref: " >> out}

                 { if ($20) {
                   printf ("                      - \047@type\047: syn\n") >> out;
                   printf ("                        \047#text\047: \"%s\"\n", $20) >> out;

                 }
                 }

                 { if ($21) {
                   printf ("                      - \047@type\047: syn\n") >> out;
                   printf ("                        \047#text\047: \"%s\"\n", $21) >> out;

                 }
                 }

                 { if ($22) {
                   printf ("                      - \047@type\047: syn\n") >> out;
                   printf ("                        \047#text\047: \"%s\"\n", $22) >> out;
                 }
                 }

                 { print  "                    categ: " >> out}
                 { if ($64 ~ /dp/)
                   { print "                       - dp" >> out}
                 }

                 { print  "                - " "\047" "@xml:lang" "\047" ": bm" >> out}
                 { print  "                  def: " >> out}
                 { print  "                    deftext: " $22 >> out} 

                 { if ($7) {
                   printf  "            sr: \n" >> out;
                   printf  "              kref: \n" >> out;
                   printf ("              - \047@type\047: spv\n") >> out;
                   printf ("                \047#text\047: \"%s\"\n", $7) >> out;
                 }
                 }

                 { if ($8) {
                   printf ("              - \047@type\047: spv\n") >> out;
                   printf ("                \047#text\047: \"%s\"\n", $8) >> out;
                 }
                 }' $tsv 

#                   cmd="uuidgen"
#
#awk -v cmd="$cmd" -v uuid="$($cmd)" '
#BEGIN {FS=OFS="\t"}
#$2 ~ /^[[:blank:]]*$/ {
#   $2 = ((cmd | getline out) > 0 ? out : uuid)
#   close(cmd)
#} 1' file | column -t
