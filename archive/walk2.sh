#!/usr/bin/bash
#works for def elements
yq -px -oj ./wtlex/wtlex.xml >./wtlex.json
jq 'def ensure_def_is_array:
    if type == "object" then
        . as $in
        | reduce keys[] as $key
          ({}; . + { ($key): ($in[$key] | ensure_def_is_array) })
        | if has("def") then
              .def |= if type == "array" then map(ensure_def_is_array) else [ensure_def_is_array] end
          else .
          end
    elif type == "array" then
        map(ensure_def_is_array)
    else .
    end;

walk(if type == "object" and has("def") then ensure_def_is_array else . end)' <wtlex.json >output.json
