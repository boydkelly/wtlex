#!/usr/bin/bash

yq -oy '. | select (.xdxf.lexicon.ar[])
| .def |= ([] + .)
| .def[].def |= ([] + .)
| .def[].def[].def |= ([] + .)
| .def[].def[].def[].def |= ([] + .)
| .def[].def[].def[].def[].ex |= ([] + .)' \
  wtlex/wtlex.json

yq -oy '. | select (.xdxf.lexicon.ar[])
| .def |= (.def[] + .)
| .def[].def |= (.def[].def[] + .)
| .def[].def[].def |= ([] + .)
| .def[].def[].def[].def |= ([] + .)
| .def[].def[].def[].def[].ex |= ([] + .)' \
  wtlex/wtlex.xml >test2.yaml

#yq -oy '. | select (.xdxf.lexicon.ar[].k[].def[]) |= ([] + .)'
#|

yq -oj '.[] | select (.xdxf.lexicon.ar[])
| .def |= ([] + .)
| .def[].def |= ([] + .)
| .def[].def[].def |= ([] + .)
| .def[].def[].def[].def |= ([] + .)
| .def[].def[].def[].def[].ex |= ([] + .)' \
  wtlex/wtlex.xml >test.json

jq '.xdxf.lexicon.ar[].def.def |= [.]' <wtlex/wtlex.json | yq -pj

jq '.xdxf.lexicon.ar[].def.def |= [.]' <wtlex/wtlex.json | jq .xdxf.lexicon.ar[].def.def.def[]
