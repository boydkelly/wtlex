#!/usr/bin/bash
root=lexique
file=headwords

for x in xml xsd yaml adoc json; do export "${x}=${file}.$x"; done

cat << EOF > ${xml}
<?xml version='1.0' encoding='utf-8'?>
<$root xmlns="http://www.coastsystems.net"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.coastsystems.net $xsd"
>
EOF

cat $yaml | yq -x --xml-root=deleteme | sed '/deleteme/d' >> $xml

echo "</$root>" >> $xml
xmllint --noout --schema $xsd $xml

cat $yaml | yq > $json 
#xslt3 -json:$json -xsl:sort2.xsl
