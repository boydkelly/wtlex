validate() {
  echo Validating "$1" filetype="${1##*.}"
  if [ "${1##*.}" == "tsv" ]; then
    echo tsv
    csvclean -n -v -t "$1"
    awk 'BEGIN{FS="\t"} !n{n=NF}n!=NF{rc=1;print "Error line " NR " " NF " fields"; exit rc} END{print NF " fields" }' "$1"
  else
    csvclean -n -v "$1"
    awk 'BEGIN{FS=","} !n{n=NF}n!=NF{rc=1;print "Error line " NR " " NF " fields"; exit rc} END{print NF " fields" }' "$1"
  fi
  echo "$(wc -l "$1")" lines in "$1"
  #delete missing field 1
  #  sed -i /^,/d $1
  #remove any defs with discontined verbs for now
  #  sed -i /[...]/d $1
}

count() {
  # cat $1 | awk -v file=$1 '{n += gsub(/\<ar\>/, ")} END {print n " entries in " file}'
  #num_elements=$(yq '.xdxf.lexicon.ar | length' $input_file)
  # echo $(sed -n '/<ar>/p' $1 | wc -l) entries in "$1"
  echo "$(yq '.xdxf.lexicon.ar | length' "$1") headwords in $1"
}

count_xml() {
  # cat $1 | awk -v file=$1 '{n += gsub(/\<ar\>/, ")} END {print n " entries in " file}'
  # echo $(sed -n '/<ar>/p' $1 | wc -l) xml headwords in "$1"
  echo "$(yq '.xdxf.lexicon.ar | length' "$1") xml headwords in $1"
  echo "$(sed -n '/<\/ex_or/p' "$1" | wc -l)" xml examples in "$1"
}

count_yml() {
  echo "$(yq '.xdxf.lexicon.ar | length' "$1") yml headwords in $1"
  # echo $(sed -n '/\- k/p' $1 | wc -l) yaml headwords in "$1"
  echo $(sed -n '/\ex_or/p' $1 | wc -l) yaml examples in "$1"
}

count_tsv() {
  echo $(wc -l "$1") records in "$1"
}

yq() {
  /home/bkelly/.local/bin/yq "$@"
}
export -f yq
set -o errexit

checkbranch() {
  local project="$1"
  local branch="$2"
  git -C "${project}" branch --show-current
  git -C "${project}" checkout "${branch}" || {
    echo "checkout ${project} $branch failed"
    exit 1
  }
}
