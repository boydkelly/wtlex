<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!-- Identity transform to copy all nodes by default -->
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
  </xsl:template>

  <!-- Template to match the <ar> element and perform the swap -->
  <xsl:template match="/xdxf/lexicon/ar">
    <xsl:copy>
      <!-- Apply templates to all children except <k> and <def> -->
      <xsl:apply-templates select="@*|node()[not(self::k or self::def)]" />

      <!-- Swap the values of <k> and <deftext> -->
      <k>
        <xsl:value-of select="def/def/def[1]/def/deftext" />
      </k>

      <def>
        <xsl:apply-templates select="def/@*" />
        <xsl:apply-templates select="def/*[name() != 'def']" />
        <xsl:apply-templates select="def/def[1]">
          <xsl:with-param name="new-deftext" select="k" />
        </xsl:apply-templates>
        <xsl:apply-templates select="def/def[position() > 1]" />
      </def>
    </xsl:copy>
  </xsl:template>

  <!-- Template to update the <deftext> value -->
  <xsl:template match="def/def/def[1]/def/deftext">
    <xsl:param name="new-deftext" />
    <deftext>
      <xsl:value-of select="$new-deftext" />
    </deftext>
  </xsl:template>

</xsl:stylesheet>

