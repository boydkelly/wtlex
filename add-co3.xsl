<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" indent="yes" />

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="/xdxf/lexicon/ar/def/def">
    <xsl:copy>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates select="gr" />
      <co type="uuid">
        <xsl:value-of select="./@def-id" />
      </co>
      <xsl:apply-templates select="* except gr" />
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>

