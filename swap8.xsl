<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="3.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" indent="yes" />

  <xsl:mode on-no-match="shallow-copy" />

  <xsl:template match="ar">
    <xsl:variable name="deftext-value" select="def/def/def[1]/def/deftext" />

    <xsl:copy>
      <xsl:apply-templates select="@*" />
      <k xml:lang="fr">
        <xsl:value-of select="$deftext-value" />
      </k>
      <xsl:apply-templates select="def">
        <xsl:with-previous-sibling test="self::def[position() = 1]">
          <xsl:when test="not(current())">
            <xsl:element name="def">
              <xsl:copy-of select="@*" />
              <xsl:apply-templates select="deftext | node()[not(self::deftext)]" />
            </xsl:element>
          </xsl:when>
        </xsl:with-previous-sibling>
      </xsl:apply-templates>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="deftext">
    <deftext>
      <xsl:value-of select="ancestor::ar/k" />
    </deftext>
  </xsl:template>

</xsl:stylesheet>

