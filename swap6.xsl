<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="3.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" indent="yes" />

  <!-- Identity transform -->
  <xsl:mode on-no-match="shallow-copy" />

  <!-- Template to swap values -->
  <xsl:template match="ar">
    <xsl:variable name="deftext-value" select="def/def/def[1]/def/deftext" />

    <xsl:copy>
      <xsl:apply-templates select="@*" />
      <!-- Swap k and deftext values -->
      <k xml:lang="fr">
        <xsl:value-of select="$deftext-value" />
      </k>
      <xsl:apply-templates select="node()[not(self::k)]" />
    </xsl:copy>
  </xsl:template>

  <!-- Template to update def/def/def[1] -->
  <xsl:template match="def/def/def[1]">
    <xsl:copy>
      <xsl:attribute name="xml:lang">dyu</xsl:attribute>
      <xsl:apply-templates select="@*[not(name()='xml:lang')]" />
      <xsl:apply-templates select="node()" />
    </xsl:copy>
  </xsl:template>

  <!-- Template to update deftext within def/def/def[1] -->
  <xsl:template match="def/def/def[1]/def/deftext">
    <deftext>
      <xsl:value-of select="ancestor::ar/k" />
    </deftext>
  </xsl:template>

</xsl:stylesheet>

