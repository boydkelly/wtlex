#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail

# Input and output file
input_file="./wtlex/wtlex.json"
output_file="$HOME/dev/svelte/coastsystems.net/src/assets/metadata/wtlex.json"
# output_file="wtlex.json"
# Create a copy of the input file to work on
#cp $input_file $output_file
yq -pj -oj $input_file >"$output_file"
# Get the number of elements in the array
num_elements=$(yq '.xdxf.lexicon.ar | length' $input_file)
echo "$num_elements"
# Loop through each element and insert a unique UUID
for i in $(seq 0 $((num_elements - 1))); do
  UUID=$(uuidgen) && export UUID
  #   yq -i -oj '.xdxf.lexicon.ar[].def |= (.id = (env(UUID)))' $output_file
  #  yq -i -oj ".xdxf.lexicon.ar[$i].def.id = $i" $output_file
  # echo "$i"
  yq -i -pj -oj ".xdxf.lexicon.ar[$i] |= (.id = $i)" "$output_file"
  # yq -i -oj ".xdxf.lexicon.ar[$i] |= (.id = (env(UUID)))" "$output_file"
  #echo "$UUID"
done

# Print the result
tail -50 "$output_file"

num_elements=$(yq -pj '.xdxf.lexicon.ar | length' "$output_file")
echo "$num_elements"
