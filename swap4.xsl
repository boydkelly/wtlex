<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" indent="yes" />

  <!-- IdentityTransform -->
  <xsl:template match="/">
    <xsl:apply-templates select="@*|node()" />
  </xsl:template>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
  </xsl:template>

  <!-- Template to swap values -->
  <xsl:template match="ar">
    <xsl:variable name="deftext-value" select="def/def/def[1]/def/deftext[1]" />

    <xsl:copy>
      <xsl:apply-templates select="@*" />
      <!-- Swap k and deftext values -->
      <k xml:lang="fr">
        <xsl:value-of select="$deftext-value" />
      </k>
      <xsl:apply-templates select="node()[not(self::k)]" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="ar/def/def/def">
    <xsl:choose>
      <xsl:when test="position() = 1">
        <def xml:lang="dyu">
          <xsl:apply-templates select="@* | node()" />
        </def>
      </xsl:when>
      <xsl:otherwise>
        <def>
          <xsl:apply-templates select="@* | node()" />
        </def>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Template to update deftext within def/def/def[1] -->
  <xsl:template match="ar/def/def/def[1]/def/deftext">
    <deftext>
      <xsl:value-of select="ancestor::ar/k" />
    </deftext>
  </xsl:template>

</xsl:stylesheet>

