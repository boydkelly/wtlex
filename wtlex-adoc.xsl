<?xml version="1.0"?>
<xsl:stylesheet version="2.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
>
<xsl:output method="text" encoding="UTF-8" />
<xsl:param name="asciidoctor" select="'a| '" />
<xsl:param name="asterisk" select="'* '" />
<xsl:param name="delim" select="','" />
<xsl:param name="doublecolon" select="'::'" />
<xsl:param name="level1" select="'== '" />
<xsl:param name="level2" select="'=== '" />
<xsl:param name="level3" select="'==== '" />
<xsl:param name="newline" select="'&#10;'" />
<xsl:param name="parl" select="'('" />
<xsl:param name="parr" select="')'" />
<xsl:param name="plus" select="' +'" />
<xsl:param name="period" select="'. '" />
<xsl:param name="right_bottom" select="'>.>| '" />
<xsl:param name="separator" select="'|'" />
<xsl:param name="tdef" select="'|==='" />
<xsl:strip-space elements="*" />

<xsl:key name="initialChar" match="xdxf/lexicon/ar" use="translate(substring(k[1],1,1),
'aàábcdeéɛfghiìíjklmnɲŋoòóɔpqrstuvwxyz',
'AAABCDEEƐFGHIIIJKLMNƝŊOÒÓƆPQRSTUVWXYZ')" />

<!-- IdentityTransform -->
<xsl:template match="/xdxf/lexicon">
  <xsl:apply-templates select="@*|node()"/>
</xsl:template>

<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="/xdxf/lexicon"/>
  </xsl:copy>
</xsl:template>

<xsl:template match="/xdxf/lexicon">
  <xsl:variable name="ars" select="ar" />
  <xsl:variable name="defs" select="/def/def" />
  <xsl:variable name="grs" select="$defs/gr" />

  <!-- header -->
  <xsl:text>:leveloffset: -1</xsl:text>
  <xsl:value-of select="$newline" />
  <xsl:text>:page-partial:</xsl:text>
  <xsl:value-of select="$newline" />
  <xsl:value-of select="$newline" />
  <xsl:text>toc&#58;&#58;[]</xsl:text>
  <xsl:value-of select="$newline" />
  <xsl:value-of select="$newline" />


  <xsl:for-each select="//ar[generate-id(.)=generate-id(key('initialChar', 
  translate(substring(k[1],1,1),
  'aàábcdeéɛfghiìíjklmnɲŋoòóɔpqrstuvwxyz',
  'AAABCDEEƐFGHIIIJKLMNƝŊOÒÓƆPQRSTUVWXYZ'))[1])]">

  <!-- sort here -->
  <xsl:sort select="k[1]" lang="en" data-type="text" order="ascending" />
  <xsl:variable name="myChar" select="translate(substring(k[1],1,1),
  'aàábcdeéɛfghiìíjklmnɲŋoòóɔpqrstuvwxyz',
  'AAABCDEEƐFGHIIIJKLMNƝŊOÒÓƆPQRSTUVWXYZ')" />

  <!-- output the level1 header  -->
  <xsl:value-of select="$newline" />
  <xsl:value-of select="concat($level2, string($myChar))" /> 
  <xsl:value-of select="$newline" />
  <xsl:value-of select="$newline" />
  <xsl:text>toc&#58;&#58;[]</xsl:text>
  <xsl:value-of select="$newline" />

  <!-- iterate over all the unique initial letter values -->
  <xsl:value-of select="$newline" />
  <!--   The defs go here    -->
  <xsl:for-each select="key('initialChar',$myChar)" >
    <xsl:sort select="k[1]" />

    <xsl:value-of select="concat($level3, ' ')" /><xsl:value-of select="k" separator=" | "/>
    <xsl:value-of select="$newline" />
    <xsl:value-of select="$newline" />

    <!--   this contains the gr  ie for each n, v, adj  -->
    <xsl:for-each select="def/def">

      <xsl:variable name="countSPV" select="count(../sr/kref[@type='spv'])"/>
      <xsl:if test="$countSPV &gt; 0" > 
        <xsl:value-of select="$parl" />
        <xsl:value-of select="../sr/kref[@type='spv']" separator=" | " />
        <xsl:value-of select="$parr" />
      </xsl:if>

      <!--   this contains the possible multiple defs for n v adj -->
      <!--  defs level 3  -->
      <xsl:for-each select="def[@xml:lang='fr']">

        <xsl:value-of select="$newline" />
        <xsl:value-of select="$newline" />
        <xsl:if test="gr/abbr != '' and gr/abbr != 'rel'">
          <xsl:text>[]</xsl:text>
          <xsl:value-of select="$newline" />
          <xsl:value-of select="concat($asterisk, gr/abbr)" />
          <xsl:value-of select="$newline" />
        </xsl:if>

        <!--   Only number defs when more than one    -->
        <xsl:variable name="onedef" select="count(def/deftext)" />

        <!--  defs level 4  -->
        <xsl:for-each select="def">
          <xsl:if test="$onedef &gt; 1" >
            <xsl:value-of select="concat($period, '_', deftext, '_')" />
          </xsl:if>
          <xsl:if test="$onedef = 1" >
            <xsl:value-of select="$newline" />
            <xsl:value-of select="$newline" />
            <xsl:text>[none]</xsl:text>
            <xsl:value-of select="$newline" />
            <xsl:value-of select="concat($asterisk, '_', deftext, '_')" />
          </xsl:if>

          <xsl:value-of select="$newline" />
          <xsl:value-of select="$newline" />

          <xsl:for-each select="ex">
            <xsl:if test="ex_orig != ''">
              <xsl:value-of select="normalize-space(concat(ex_orig,$doublecolon))"/>
              <xsl:value-of select="$newline" />
              <xsl:value-of select="normalize-space(ex_tran)"/>
              <xsl:value-of select="$newline" />
            </xsl:if>
          </xsl:for-each>

          <xsl:value-of select="$newline" />
          <xsl:variable name="countSYN" select="count(sr/kref[@type='syn'])"/>
          <xsl:if test="$countSYN &gt; 0 and not(normalize-space(sr/kref[@type='syn'][1])='')" > 
            <xsl:text>cf. </xsl:text>
            <xsl:value-of select="sr/kref[@type='syn']" separator=" | " />
          </xsl:if>
          <xsl:value-of select="$newline" />
          <xsl:value-of select="$newline" />

          <xsl:if test="../../def[@xml:lang='bm']/def/deftext != ''">
            <xsl:value-of select="concat($asterisk, 'bm: ', '_', ../../def[@xml:lang='bm']/def/deftext, '_')" />
            <xsl:value-of select="$newline" />
          </xsl:if>

          <xsl:value-of select="$newline" />
        </xsl:for-each>

        <xsl:value-of select="$newline" />
      </xsl:for-each>

      <xsl:value-of select="$newline" />
    </xsl:for-each>

    <xsl:value-of select="$newline" />
  </xsl:for-each>

</xsl:for-each>
</xsl:template>

</xsl:stylesheet>
